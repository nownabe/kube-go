package main

import (
	"fmt"
	"io/ioutil"

	_ "k8s.io/client-go/plugin/pkg/client/auth"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/kubernetes/pkg/kubectl/cmd/util/openapi"
	utilvalid "k8s.io/kubernetes/pkg/kubectl/cmd/util/openapi/validation"
)

func main() {
	// SerializeとDeserializeの方法を定義するやつ
	// https://godoc.org/k8s.io/apimachinery/pkg/runtime#Scheme
	scheme := runtime.NewScheme()

	// Deserializerとかをくれるやつ
	// https://godoc.org/k8s.io/apimachinery/pkg/runtime/serializer#CodecFactory
	codecFactory := serializer.NewCodecFactory(scheme)

	// Deserializer
	// https://godoc.org/k8s.io/apimachinery/pkg/runtime#Decoder
	deserializer := codecFactory.UniversalDeserializer()

	fmt.Println("==== namespace.yaml ====")

	namespaceYAML, err := ioutil.ReadFile("namespace.yaml")
	if err != nil {
		panic(err)
	}

	namespaceObject, _, err := deserializer.Decode(namespaceYAML, nil, &corev1.Namespace{})
	if err != nil {
		panic(err)
	}
	namespace := namespaceObject.(*corev1.Namespace)
	fmt.Printf("Name: %s\n", namespace.ObjectMeta.GetName())
	fmt.Println("==== validation ====")
	fmt.Println(validate(namespaceYAML))
	fmt.Println()

	fmt.Println("==== deployment.yaml ====")

	deploymentYAML, err := ioutil.ReadFile("deployment.yaml")
	if err != nil {
		panic(err)
	}

	deploymentObject, _, err := deserializer.Decode(deploymentYAML, nil, &appsv1.Deployment{})
	if err != nil {
		panic(err)
	}
	deployment := deploymentObject.(*appsv1.Deployment)
	fmt.Printf("Namespace: %s\n", deployment.ObjectMeta.GetNamespace())
	fmt.Printf("Resources: %+v\n", deployment.Spec.Template.Spec.Containers[0].Resources.Limits["cpu"])
	fmt.Println("==== validation ====")
	fmt.Println(validate(deploymentYAML))
	fmt.Println()

	fmt.Println("==== invalid-deployment.yaml ====")

	invalidYAML, err := ioutil.ReadFile("invalid-deployment.yaml")
	if err != nil {
		panic(err)
	}

	invalidObject, _, err := deserializer.Decode(invalidYAML, nil, &appsv1.Deployment{})
	if err != nil {
		panic(err)
	}
	invalid := invalidObject.(*appsv1.Deployment)
	fmt.Printf("Namespace: %s\n", invalid.ObjectMeta.GetNamespace())
	fmt.Printf("Resources: %+v\n", invalid.Spec.Template.Spec.Containers[0].Resources.Limits["cpu"])

	fmt.Println("==== validation ====")
	fmt.Println(validate(invalidYAML))
}

func validate(data []byte) error {
	config := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		clientcmd.NewDefaultClientConfigLoadingRules(),
		&clientcmd.ConfigOverrides{},
	)
	clientConfig, err := config.ClientConfig()
	if err != nil {
		return err
	}

	discoveryClient, err := discovery.NewDiscoveryClientForConfig(clientConfig)
	if err != nil {
		return err
	}

	/*
		configFlags := genericclioptions.NewConfigFlags()
		discovery, err := configFlags.ToDiscoveryClient()
		if err != nil {
			return err
		}
	*/

	getter := openapi.NewOpenAPIGetter(discoveryClient)
	resources, err := getter.Get()
	if err != nil {
		return nil
	}

	validator := utilvalid.NewSchemaValidation(resources)

	return validator.ValidateBytes(data)
}
